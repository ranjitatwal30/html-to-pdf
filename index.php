<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
$input = json_decode(file_get_contents('php://input'),true);

//print_r($input);die;


$job_id = $input['job_id'];
$html = urldecode($input['html']);
$html_url = $job_id.".html";



if($job_id){

    $pdf_url = $job_id.".pdf";

                    // create output file for conversion result
                    $output_file = fopen($html_url, "wb");

                    // check for a file creation error
                    if (!$output_file)
                        throw new \Exception(error_get_last()['message']);

                    // write the pdf into the output file
                    $written = fwrite($output_file, $html);

                    // check for a file write error
                    if ($written === false)
                        throw new \Exception(error_get_last()['message']);

                    // close the output file
                    fclose($output_file);

    $output = exec("xvfb-run wkhtmltopdf --margin-top 0 --margin-bottom 0 --margin-left 0 --margin-right 0 $html_url $pdf_url");


    if(file_exists($pdf_url)){

        $pdf_content = file_get_contents($pdf_url);


        $return = array(
            "status" => "success",
            "message" => "HTML successfully converted to PDF",
            "data" => $pdf_content
        );
    }else{
        $return = array(
            "status" => "error",
            "message" => "Error converting html to pdf file",
            "data" => $output
        );
        header('Content-Type: application/json');
        echo json_encode($return);die;
    }
    
    if(file_exists($pdf_url)){
        unlink($pdf_url);
    }
    if(file_exists($html_url)){
        unlink($html_url);
    }
    
    echo $return['data'];die;
}else{
    header('Content-Type: application/json');
    $return = array(
        "status" => "error",
        "message" => "job id is missing",
    );
    echo json_encode($return);die;
}



